package edu.neo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name="empleados")

public class EmpleadoEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_empleado;
	
	@Column
	private String nombre;
	
	@Column
	private String apellido;
	
	@Column
	private int dni;

	public int getId() {
		return id_empleado;
	}

	public void setId(int id) {
		this.id_empleado = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}
}
