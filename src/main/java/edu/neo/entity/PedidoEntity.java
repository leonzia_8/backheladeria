package edu.neo.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TemporalType;
import javax.persistence.Temporal;

//import org.springframework.data.jpa.repository.Temporal;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name="pedidos")

public class PedidoEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
//	id_pedidos	id_empleado	cono	vaso_1/4	pote_1kg	topping	fecha
	@Column
	private int id_pedidos;
	@Column
	private int id_empleado;
	@Column
	private int cono;
	@Column
	private int vaso_cuarto;
	@Column
	private int pote_kilo;
	@Column
	private int topping;
	@Column
	@Temporal(TemporalType.DATE)
    private Date fecha;
	
	
	public int getId_pedidos() {
		return id_pedidos;
	}
	public void setId_pedidos(int id_pedidos) {
		this.id_pedidos = id_pedidos;
	}
	public int getId_empleado() {
		return id_empleado;
	}
	public void setId_empleado(int id_empleado) {
		this.id_empleado = id_empleado;
	}
	public int getCono() {
		return cono;
	}
	public void setCono(int cono) {
		this.cono = cono;
	}
	public int getVaso_cuarto() {
		return vaso_cuarto;
	}
	public void setVaso_cuarto(int vaso_cuarto) {
		this.vaso_cuarto = vaso_cuarto;
	}
	public int getPote_kilo() {
		return pote_kilo;
	}
	public void setPote_kilo(int pote_kilo) {
		this.pote_kilo = pote_kilo;
	}
	public int getTopping() {
		return topping;
	}
	public void setTopping(int topping) {
		this.topping = topping;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
}
	
	
	
	
	