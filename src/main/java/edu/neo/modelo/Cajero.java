package edu.neo.modelo;

public class Cajero {
	private int numeroCaja;
	private int cantidadPedidos;

	public Cajero(int numero, int pedidos) {
		this.numeroCaja = numero;
		this.cantidadPedidos = pedidos;
	}

	public int getNumeroCaja() {
		return numeroCaja;
	}

	public void setNumeroCaja(int numeroCaja) {
		this.numeroCaja = numeroCaja;
	}

	public int getCantidadPedidos() {
		return cantidadPedidos;
	}

	public void setCantidadPedidos() {
		this.cantidadPedidos = cantidadPedidos - 1;
	}

	public int validaCantidad() {

		int retorno;

		if (this.cantidadPedidos > 0) {
			retorno = this.cantidadPedidos;
		} else {
			retorno = 0;
		}

		return retorno;

	}

}
