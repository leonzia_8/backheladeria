package edu.neo.modelo;

import java.util.Date;

public class Pedido {

	private int id_pedidos;
	private int id_empleado;
	private int cono;
	private int vaso_cuarto;
	private int pote_kilo;
	private int topping;
	private Date fecha;

	public int getId_pedidos() {
		return id_pedidos;
	}

	public void setId_pedidos(int id_pedidos) {
		this.id_pedidos = id_pedidos;
	}

	public int getId_empleado() {
		return id_empleado;
	}

	public void setId_empleado(int id_empleado) {
		this.id_empleado = id_empleado;
	}

	public int getCono() {
		return cono;
	}

	public void setCono(int cono) {
		this.cono = cono;
	}

	public int getVaso_cuarto() {
		return vaso_cuarto;
	}

	public void setVaso_cuarto(int vaso_cuarto) {
		this.vaso_cuarto = vaso_cuarto;
	}

	public int getPote_kilo() {
		return pote_kilo;
	}

	public void setPote_kilo(int pote_kilo) {
		this.pote_kilo = pote_kilo;
	}

	public int getTopping() {
		return topping;
	}

	public void setTopping(int topping) {
		this.topping = topping;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
}
