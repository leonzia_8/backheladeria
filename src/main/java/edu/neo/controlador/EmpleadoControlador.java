package edu.neo.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.neo.entity.EmpleadoEntity;
import edu.neo.modelo.RespuestaEntity;
import edu.neo.repository.EmpleadoRepository;

@CrossOrigin
@RestController
public class EmpleadoControlador {

	// spring va a darle la instancia ( new Persona();

	@Autowired
	private EmpleadoRepository repository;
	
	@Autowired
	private RespuestaEntity respuesta;

	@GetMapping("/saludo")
	public String saludo() {
		return "hola mundo";
	}

	@GetMapping("/getAllEmpleado")
	public ResponseEntity<?> getAllEmpleado() {
		try {
			return ResponseEntity.ok(this.repository.findAll());
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("/borrarEmpleado/{id}")
	public ResponseEntity<?> deleteEmpleado(@PathVariable("id") int id_empleado) {

		try {
			
			this.repository.deleteById(id_empleado);
			this.respuesta.setMensaje("Elemento borrado");
			return ResponseEntity.ok(this.respuesta);
		} catch (Exception e) {
			this.respuesta.setMensaje("No se puede borrar el id " +  id_empleado  + ", porque no existe en la base o ya fue borrado");
			return ResponseEntity.ok(this.respuesta);
		}
	}
//
	@GetMapping("empleado/{id}")
	public ResponseEntity<?> getEmpleado(@PathVariable("id") int id_empleado) {
		try {
			
			return ResponseEntity.ok(this.repository.findById(id_empleado).get());
		} catch (Exception e) {
			this.respuesta.setMensaje("id " + id_empleado +  ", no encontrado");
			return ResponseEntity.ok(this.respuesta);
		}
	}

	
	@GetMapping("/empleadoRandom")
	public ResponseEntity<?> getEmpleadoRandom() {

		
		Long qty = this.repository.count()+1;
		int idx = (int)(Math.random()*(qty-1+1)+1);

		try {
			return ResponseEntity.ok(this.repository.findById(idx).get());
		} catch (Exception e) {
			this.respuesta.setMensaje("id " + idx +  ", no encontrado");
			return ResponseEntity.ok(this.respuesta);
		}
	}

//	/*
//	 * nombre: "pepeito", apellido:"mengano", edad:22
//	 */
	@PostMapping("/saveEmpleado")
	public ResponseEntity<?> saveEmpleado(@RequestBody() EmpleadoEntity empleado) {

		this.respuesta.setMensaje("Elemento guardado!");
		this.repository.save(empleado);
		return ResponseEntity.ok(this.respuesta);
	}

}