package edu.neo.controlador;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import edu.neo.modelo.Cajero;

@CrossOrigin
@RestController
public class CajeroControlador {

	Cajero cajero1 = new Cajero(1, 5);
	Cajero cajero2 = new Cajero(2, 10);
	Cajero cajero3 = new Cajero(3, 15);
//	@Autowired
//	private RespuestaEntity respuesta;

	@GetMapping("/getAllCajeros")
	public String getAllCajeros() {
		return cajero1.getCantidadPedidos() + " " + cajero2.getCantidadPedidos() + " " + cajero3.getCantidadPedidos();
	}

	@GetMapping("/abrirHeladeria")
	public String abrirHeladeria() {
		String retorno = "";
		if (cajero1.getCantidadPedidos() == 0 && cajero2.getCantidadPedidos() == 0
				&& cajero3.getCantidadPedidos() == 0) {
			cajero1 = new Cajero(1, 5);
			cajero2 = new Cajero(2, 10);
			cajero3 = new Cajero(3, 15);
			retorno = "Cajas activadas";
		} else {
			retorno = "Debe agotar los pedidos de las 3 cajas.";
		}

		return retorno;
	}

	@GetMapping("/cajeros/{id}")
	public int getCajero(@PathVariable("id") int numeroCaja) {
		int retorno = 0;

		if (numeroCaja == 1) {
			cajero1.setCantidadPedidos();
			retorno = cajero1.validaCantidad();
		}
		if (numeroCaja == 2) {
			cajero2.setCantidadPedidos();
			retorno = cajero2.validaCantidad();
		}
		if (numeroCaja == 3) {
			cajero3.setCantidadPedidos();
			retorno = cajero3.validaCantidad();
		}

		if (cajero1.validaCantidad() == 0 && cajero2.validaCantidad() == 0 && cajero3.validaCantidad() == 0) {
			retorno = 10000;

		}

		return retorno;

	}

}