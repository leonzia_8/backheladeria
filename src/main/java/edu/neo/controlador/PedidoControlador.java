package edu.neo.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.neo.entity.PedidoEntity;
import edu.neo.modelo.RespuestaEntity;
import edu.neo.repository.PedidoRepository;

@CrossOrigin
@RestController
public class PedidoControlador {

	// spring va a darle la instancia ( new Persona();

	@Autowired
	private PedidoRepository repository;
	
	@Autowired
	private RespuestaEntity respuesta;

//	@GetMapping("/saludo")
//	public String saludo() {
//		return "hola mundo";
//	}

	@GetMapping("/getAllPedido")
	public ResponseEntity<?> getAllPedido() {
		try {
//			System.out.println("all pedidos ok");			
			return ResponseEntity.ok(this.repository.findAll());
		} catch (Exception e) {
			System.out.println("error" + e);

			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("/borrarPedido/{id}")
	public ResponseEntity<?> deletePedido(@PathVariable("id") int id_pedido) {
		System.out.println("borrra pedidos");
		try {
			
			this.repository.deleteById(id_pedido);
			this.respuesta.setMensaje("Elemento borrado");
			return ResponseEntity.ok(this.respuesta);
		} catch (Exception e) {
			this.respuesta.setMensaje("No se puede borrar el id " +  id_pedido  + ", porque no existe en la base o ya fue borrado");
			return ResponseEntity.ok(this.respuesta);
		}
	}
//
	@GetMapping("pedido/{id}")
	public ResponseEntity<?> getPedido(@PathVariable("id") int id_pedidos) {
		System.out.println("pedido x id");
		try {
			
			return ResponseEntity.ok(this.repository.findById(id_pedidos).get());
		} catch (Exception e) {
			this.respuesta.setMensaje("id " + id_pedidos +  ", no encontrado");
			return ResponseEntity.ok(this.respuesta);
		}
	}

//	/*
//	 * nombre: "pepeito", apellido:"mengano", edad:22
//	 */
	@PostMapping("/savePedido")
	public ResponseEntity<?> savePedido(@RequestBody() PedidoEntity pedido) {

		this.repository.save(pedido);
		this.respuesta.setMensaje("Elemento guardado!");
		return ResponseEntity.ok(this.respuesta);
	}

}