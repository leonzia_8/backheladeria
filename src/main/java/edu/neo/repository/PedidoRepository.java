package edu.neo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.neo.entity.PedidoEntity;
import org.springframework.data.jpa.repository.Temporal;

public interface PedidoRepository extends JpaRepository<PedidoEntity, Integer>{

}
