package edu.neo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.neo.entity.EmpleadoEntity;

public interface EmpleadoRepository extends JpaRepository<EmpleadoEntity, Integer> {

}
